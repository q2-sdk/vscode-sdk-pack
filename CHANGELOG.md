# Change Log

All notable changes to the "vscode-sdk-pack" extension pack will be documented in this file.

## [0.4.0]

- Adds Ruff extension

## [0.3.0]

- Adds Pylance extension

## [0.2.0]

- Adds better Jinja extension

## [0.1.0]

- Initial release
